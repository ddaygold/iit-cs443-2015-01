(ns grammars.core
  (:require [clojure.core.typed :as t])
  (:gen-class))

(def ex1 {'S [['x 'S 'x]
              [:eps]]})

(def ex2 {'S [['A 'x]
              ['B 'y]
              ['z]]
          'A [['1 'C 'B]
              ['2 'B]]
          'B [['3 'B]
              ['C]]
          'C [['4]
              [:eps]]})

(def fs1 {'S {:terms #{'x}
              :eps false}
          'E {:terms #{'y}
              :eps true}})

(defn join-fs [fs1 fs2]
  {:terms (into (:terms fs1) (:terms fs2))
   :eps (and (:eps fs1) (:eps fs2))}) 

(defn join-fs-eps [fs1 fs2]
  {:terms (into (:terms fs1) (:terms fs2))
   :eps (or (:eps fs1) (:eps fs2))})

(defn first-set-of [rule fs-map]
  (loop [r rule
         acc  {:terms [] :eps true}]
    (if (empty? r) acc
        (let [s (first r)
              fs (fs-map s)]
          (cond ;; No more symbols
                (nil? s)  acc
                ;; Symbol is epsilon
                (= :eps s)
                (join-fs acc {:terms #{} :eps true})
                ;; Symbol is terminal.
                (nil? fs) (join-fs acc {:terms #{s} :eps false})
                ;; Epsilon.. go down the line
                (:eps fs) (recur (rest rule)
                                 (join-fs acc fs))
                ;; No epsilon.  Join and go home.
                :elsa     (join-fs acc fs))))))

(defn gen-first-set [grm]
  ;;generate an empty first set to build up
  (loop [fs 
         (reduce #(assoc %1 %2 {:terms #{} :eps false})
                 {}
                 (keys grm))]
    ;;build a new fs
    (let [nu-fs 
          (reduce (fn [fs1 sym]
                    (reduce (fn [fs2 rule]
                              (update-in fs2 [sym] #(join-fs-eps %
                                                                 (first-set-of rule fs2))))
                            fs1
                            (grm sym)))  
                  fs
                  (keys fs))]
      (if (= fs nu-fs) nu-fs
          (recur nu-fs)))))

(declare get-follows-for-sym-map)

(defn get-follow-set [sym grammar first-sets existing]
  (loop [terminals #{}, syms (keys grammar)]
    (let [curr-sym (first syms)]
      (if (empty? syms)
        terminals
        (recur (get-follows-for-sym-map sym curr-sym first-sets grammar terminals) (rest syms)))))
  )

(defn extract-from-grammar-line [sym curr-sym vects first-sets grammar terminals]
  (loop [ter terminals, curr-vect (first vects)]
    (cond
      (= (first curr-vect) sym)
      (cond
        (empty? (rest curr-vect)) (clojure.set/union ter (:terms (get-follow-set curr-sym grammar first-sets #{})))
        (:eps ((first (rest curr-vect)) first-sets)) (recur (clojure.set/union ter (:terms (get-follow-set curr-sym grammar first-sets #{}))) (rest curr-vect))
        (:match) (recur (clojure.set/union ter (:terms ((first (rest curr-vect)) first-sets))) (rest curr-vect))
        )
      (empty? curr-vect) ter
      (:else) (recur ter (rest curr-vect))
      )))

(defn get-follows-for-sym-map [sym curr-sym first-sets grammar ter]
  (loop [t ter, vects (grammar curr-sym)]
    (if (empty? vects) t
        (recur
         (extract-from-grammar-line sym curr-sym (first vects) first-sets grammar t) (rest vects))))
  )



(defn gen-follow-set [grm]
  ;; put $ in follow(start) == follow(S)
  ;; X -> aYb :
  ;;          follow(Y) += (disunion FIRST(b) :epsilon)
  ;; X -> aY || X -> aYb where (in FIRST(b) :epsilon)
  ;;          follow(Y) += FOLLOW(X)

  ;; A follow set should be a map with keys = LHS of the grammar production,
  ;; and vals = #{}'s
  ;; I'm also going to put $ in the start follow set in this step
  nil
  )

